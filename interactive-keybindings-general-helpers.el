;;; package --- Interactive-keybinding-general-helpers
;;; Commentary:
;; -*- lexical-binding: t; -*-

;;; Code:

(require 'general)
(require 'cl-lib)
(require 'interactive-keybindings)
(require 's)
(require 'dash)

(defcustom interactive-keybindings-general-out-file (f-join
                                                     user-emacs-directory
                                                     "general-out.el")
  "File which stores the output of running interactive-keybindings-general-helpers-convert-file-keybindings."
  :type 'file
  :group 'interactive-keybindings)

(defclass
  interactive-keybindings-general-helpers-keybinding
  (interactive-keybindings-interactive-keybinding)
  ((states
    :initarg :states :type list
    :documentation "States in which this key should be bound.")))

(defun interactive-keybindings-general-helpers--keybinding-to-general-helpers-keybinding (ikbd states)
  "Translate IKBD and STATES into a general-helpers-keybinding."
  (interactive-keybindings-general-helpers-keybinding
   :key-sequence (oref ikbd key-sequence)
   :command (oref ikbd command)
   :keymaps (oref ikbd keymaps)
   :states (mapcar #'intern states)))

(defun interactive-keybindings-general-helpers--get-general-keybindings ()
  "Gets the general keybindings from `interactive-keybindings-file."
  (-filter
    (lambda (kb)
      (same-class-p
       kb
       'interactive-keybindings-general-helpers-keybinding))
    (interactive-keybindings--get-keybindings-list)))

;; TODO: group keys by shared states. they must share all states to be grouped, as the states
;; are specified at the top level of the general-define-key
(defun interactive-keybindings-general-helpers--group-keybindings-by-state ()
  "Group the general keybindings from `interactive-keybindings-file by states."
  (interactive)
  (let ((by-states (make-hash-table :test 'equal))
        (kbs (interactive-keybindings-general-helpers--get-general-keybindings)))
    (cl-loop
     for
     kb
     in
     kbs
     do
     (let ((states (oref kb states)))
       (puthash
        states
        (append
         (gethash states by-states)
         `(,kb))
        by-states)))
    by-states))


(defun interactive-keybindings-general-helpers--group-keybindings-by-starter-keys ()
  "Group the general keybindings from `interactive-keybindings-file by common starter keys and states."
  )

;; TODO: this is going to have to do a lot more than the other one, but its good to get it started
;; Will want it to group together keybindings by common starter groupings of keys, keymaps, etc
;; since general allows for that structure. Although, it also works without that structure, you
;; just don't get as good which-key help. Maybe we should look into what we could do with that
;; instead of the grouping if the grouping turns out to be too complicated
(defun interactive-keybindings-general-helpers-translate-file-to-general-key-file ()
  "Convert the interactive-keybindings in `interactive-keybindings-file' to 'define-key' statements."
  (interactive)
  (let ((keybinding-list
         (-filter
          (lambda (kb)
            (same-class-p
             kb
             'interactive-keybindings-general-helpers-keybinding))
          (interactive-keybindings--get-keybindings-list))))
    (with-temp-buffer
      (erase-buffer)
      (mapc
       (lambda (keybinding)
         (let ((dk (interactive-keybindings-general-helpers--convert-keybinding
                    keybinding)))
           (when (interactive-keybindings--check-eval
                  dk)
             (insert (format "%s\n" dk)))))
       keybinding-list)
      (write-file
       interactive-keybindings-general-out-file))))


(defun general-interactive-define-key ()
  "A function which hopefully will allow me to define keybindings on the fly and store them to a file."
  (interactive)
  (let* ((ikbd (interactive-keybindings--get-keybinding))
         (states (completing-read-multiple
                  "States: "
                  (append (interactive-keybindings-general-helpers--get-states)
                          (interactive-keybindings-general-helpers--get-special-maps))
                  nil
                  t))
         (gkbd (interactive-keybindings-general-helpers--keybinding-to-general-helpers-keybinding ikbd states))
         (conflicts (interactive-keybindings-general-helpers--check-for-conflicts
                     (oref gkbd key-sequence)
                     (oref gkbd keymaps)
                     (oref gkbd states))))
    (if conflicts
        (cl-loop
         for
         conflict
         in
         conflicts
         do
         (progn
           (interactive-keybindings--keybind-conflict-to-warning
            conflict)))
      (let* ((dks (when gkbd
                    (interactive-keybindings-general-helpers--convert-keybinding gkbd)))
             (eval-results (remove
                            nil
                            (mapcar
                             #'interactive-keybindings--check-eval
                             dks))))
        (when (and dks eval-results)
          (interactive-keybindings--add-keybinding-to-file
           gkbd))))))

(defun interactive-keybindings-general-helpers--check-for-conflicts (key-sequence keymap-symbols states)
  "Check for conflicts for KEY-SEQUENCE in KEYMAP-SYMBOLS and STATES."
  ;; TODO: should probably do some checking for same states here to ensure that we don't throw false conflicts
  ;; like if we had the same key-sequence bound, but in two different states
  (let ((base-conflicts (interactive-keybindings--check-for-conflicts
                         key-sequence
                         keymap-symbols))
        (special-map-conflicts (interactive-keybindings-general-helpers--check-for-special-map-conflicts
                                key-sequence
                                states)))
    (cl-remove
     nil
     (append
      base-conflicts
      special-map-conflicts))))


(defun interactive-keybindings-general-helpers--check-for-map-conflicts (key-sequence special-map-symbol states)
  "Check `general-override-mode-map' for STATES and KEY-SEQUENCE for conflicts.
Argument SPECIAL-MAP-SYMBOL is not currently used."
  (cl-remove
   nil
   (mapcar
    (lambda (state)
      (let ((res (general--lookup-key
                  state
                  'general-override-mode-map
                  key-sequence)))
        ;; For some reason, general--lookup-key returns the number
        ;; from lookup-key only if it is greater than 1
        ;; Probably should open an issue with general to see why
        ;; and if he'd be ok with changing it
        (when (not (numberp res)) res)))
    states)))

(defun interactive-keybindings-general-helpers--check-for-special-map-conflicts (key-sequence states)
  "Check for conflicts for KEY-SEQUENCE in General's special maps for KEYMAP-SYMBOLS for STATES."
  (let* ((global-conflict-commands (interactive-keybindings-general-helpers--check-for-map-conflicts
                                    key-sequence
                                    'general-override-mode-map
                                    states))
         (local-conflict-commands  (interactive-keybindings-general-helpers--check-for-map-conflicts
                                    key-sequence
                                    'general-override-local-mode-map
                                    states))
         (global-conflicts
          (mapcar (lambda (x)
                    (interactive-keybindings-interactive-keybinding
                     :key-sequence key-sequence
                     :command (intern x)
                     :keymaps 'general-override-mode-map)) global-conflict-commands))
         (local-conflicts
          (mapcar (lambda (x)
                    (interactive-keybindings-keybind-conflict
                     :key-sequence key-sequence
                     :command (intern x)
                     :keymaps 'general-override-local-mode-map)) local-conflict-commands)))
    (append global-conflicts local-conflicts)))


(defun interactive-keybindings-general-helpers--get-special-maps ()
  "Return the names of the special maps in general.
It would be nice if these were a variable in general,
but they are not, so I'm just putting what he has listed in the README."
  '(global local))

(defun interactive-keybindings-general-helpers--get-states ()
  "Return names of states which can be used in `general--lookup-key'."
  (mapcar #'cdr general-state-aliases))

(defun interactive-keybindings-general-helpers--convert-keybinding (interactive-keybinding)
  "Turn INTERACTIVE-KEYBINDING into a 'general-define-key' list for each keymap."
  (let ((keymaps (oref interactive-keybinding keymaps))
        (command (oref interactive-keybinding command))
        (key-seq (oref interactive-keybinding key-sequence))
        (states (oref interactive-keybinding states))
        (description (oref interactive-keybinding description)))
    (cl-remove nil `(general-define-key :keymaps ',keymaps :states ',states
                                        ,key-seq ,(when (> 0 (length description))
                                                    :which-key ,description)
                                        #',command))))


;;; Tests
(general--lookup-key "visual" 'general-override-mode-map (vector 32 98 98))

(setq test-states '("normal"))

(mapcar #'intern test-states)

(setq test-binding
      (interactive-keybindings-general-helpers-keybinding
       "interactive-keybindings-general-helpers-keybinding"
       :key-sequence [32 109 101 98]
       :command (function eval-buffer)
       :keymaps '(lispy-mode-map)
       :states '(normal)))

(interactive-keybindings-general-helpers--convert-keybinding test-binding)
(interactive-keybindings--check-eval (general-define-key
                                      :keymaps (quote (lispy-mode-map))
                                      :states (quote (normal))
                                      [32 109 101 98]
                                      (function eval-buffer)))


(object-of-class-p test-binding 'interactive-keybindings-interactive-keybinding)

(interactive-keybindings-keybind-conflict
 :key-sequence (vector 32 109 101 98)
 :keymap 'lispy-mode-map
 :command (function eval-buffer)
 :keybinding test-binding)




;;; interactive-keybindings-general-helpers ends here
