;;; package --- Interactive-keybindings -*- lexical-binding: t; -*-
;;; Commentary:

;;; Code:
(require 'seq)
(require 'cl-lib)
(require 'f)
(require 'eieio)
(require 'eieio-base)
(require 'dash)

(defgroup interactive-keybindings nil
  "Group for interactive keybindings customization.")

(defcustom interactive-keybindings-quit-key-vector
  (vector "C-g")
  "Key sequence to quit the reading of key sequences to bind."
  :type 'vector
  :group 'interactive-keybindings)

(defcustom interactive-keybindings-file (f-join
                                         user-emacs-directory
                                         "interactively-defined-keybindings.el")
  "File which stores the interactively defined keybindings.
Keybindings should be of the form '(:sequence KEY-SEQUENCE :command COMMAND
:description DESCRIPTION &optional :keymap keymap), and there should be one per line."
  :type 'file
  :group 'interactive-keybindings)

(defun interactive-keybindings--command-printer (command)
  "Return COMMAND in a readable form."
  (prin1 `(function ,command)))

(defclass
  interactive-keybindings-interactive-keybinding
  ()
  (
   (key-sequence
    :initarg :key-sequence :type vector
    :documentation "Key-sequence vector to bind to.")
   (command
    :initarg :command :type (satisfies commandp)
    :printer interactive-keybindings--command-printer
    :documentation "Command to bind to key-sequence.")
   (keymaps
    :initarg :keymaps :type (satisfies
                             (lambda (x)
                               (-all-p #'symbolp x)))
    :documentation "Keymaps that this key-sequence/command combination should be bound in.")
   (description
    :initarg :description :initform ""
    :type string
    :documentation "Optional description which is likely to be used in all alternative backends."))
  "A base class for interactive-keybindings.")



(defclass
  interactive-keybindings-keybind-conflict
  ()
  ((key-sequence
    :initarg :key-sequence :type vector
    :documentation "Key-sequence vector of the conflict.")
   (keymap
    :initarg :keymap :type symbol
    :documentation "Keymap the conflict occurs in.")
   (command
    :initarg :command :type (satisfies commandp)
    :documentation "Command to which key-sequence is currently bound in keymap.")
   (keybinding
    :initarg :keybinding :type (satisfies
                                (lambda (x)
                                  (object-of-class-p
                                   x
                                   'interactive-keybindings-interactive-keybinding)))))
  "A base class for interactive-keybindings-conflicts.")

(defun interactive-keybindings-go-to-keybindings-file ()
  "Visit the file storing interactively defined keybindings."
  (interactive)
  (find-file interactive-keybindings-file))

(defun interactive-keybindings--get-keybinding (&optional extras)
  "Return an interactive-keybinding as specified by the user, with EXTRAS optionally passed in."
  (let* ((keymap-specific (y-or-n-p
                           "Keymap Specific Keybinding? "))
         (keymaps (if keymap-specific
                      (mapcar
                       #'intern
                       (completing-read-multiple
                        "Keymaps: "
                        (interactive-keybindings--get-keymaps)
                        nil
                        t))))
         (key-vector (interactive-keybindings--read-key-until
                      "Key sequence to bind: "))
         (command (completing-read
                   "Command to bind key to: "
                   obarray
                   #'commandp
                   t
                   nil
                   'extended-command-history))
         (description (read-from-minibuffer
                       "Description: "))
         (ikbd (interactive-keybindings-interactive-keybinding
                :key-sequence key-vector
                :command (intern command)
                :description (or description
                                 (format "%s" command))
                :keymaps keymaps)))
    ikbd))

;; (defun interactive-keybindings--add-keybinding-to-file (keybinding)
;;   "Append KEYBINDING to interactive-keybindings-file."
;;   (eieio-persistent-save keybinding))


(defun interactive-keybindings--add-keybinding-to-file (keybinding)
  "Append KEYBINDING to interactive-keybindings-file."
  (with-current-buffer
      (interactive-keybindings--get-buffer)
    (read-only-mode -1)
    (end-of-buffer)
    (insert (with-output-to-string (object-write keybinding)))
    (read-only-mode 1)))

(defun interactive-keybindings--keybind-conflict-to-warning (conflict)
  "Warn about CONFLICT."
  ;; Maybe use this check in the future to prompt additional actions?
  (warn
   "%s already bound to %s in %s"
   (oref conflict key-sequence)
   (oref conflict command)
   (oref conflict keymap)))

(defun interactive-keybindings--get-user-defined-conflicts (key-sequence keymap-symbols)
  "Return keybind-conflicts where KEY-SEQUENCE is already bound to something in any of KEYMAP-SYMBOLS in the interactive-keybindings-file."
  ;; This is currently coming back in a list of lists, which is not what we want
  (-flatten (cl-remove
           nil
           (mapcar
            (lambda (kb)
              (let* ((key-seq (oref kb key-sequence))
                     (command (oref kb command))
                     (kb-keymaps (oref kb keymaps))
                     (intersect (if (equal key-seq key-sequence)
                                    (cl-intersection
                                     keymap-symbols
                                     kb-keymaps))))
                (message "%s" intersect)
                (if intersect
                    (cl-loop
                     for
                     conflict-map
                     in
                     intersect
                     collect
                     (interactive-keybindings-keybind-conflict
                      :key-sequence key-sequence
                      :keymap conflict-map
                      :command command
                      :keybinding kb)))))
            (interactive-keybindings--get-keybindings-list)))))

(defun interactive-keybindings--get-other-conflicts (key-sequence keymap-symbols)
  "Return keybind-conflicts where KEY-SEQUENCE is already bound to something in any of KEYMAP-SYMBOLS."
  (-flatten (mapcar
             (lambda (km)
               (let ((conflict (lookup-key-ignore-too-long
                                (eval km)
                                key-sequence)))
                 (when conflict
                   (interactive-keybindings-keybind-conflict
                    :key-sequence key-sequence
                    :keymap km
                    :command conflict
                    :keybinding
                    (interactive-keybindings-interactive-keybinding
                     :key-sequence key-sequence
                     :keymaps km
                     :command conflict
                     :description "" )))))
             keymap-symbols)))

(defun interactive-keybindings--check-for-conflicts (key-sequence keymap-symbols)
  "Return keybind-conflicts where KEY-SEQUENCE is already bound to something in any of KEYMAP-SYMBOLS."
  (let* ((ikbd-conflicts (interactive-keybindings--get-user-defined-conflicts
                          key-sequence
                          keymap-symbols))
         (other-conflicts (interactive-keybindings--get-other-conflicts
                           key-sequence
                           keymap-symbols)))
    (cl-remove
     nil
     (append
      ikbd-conflicts
      other-conflicts))))

(defun interactive-keybindings--get-buffer ()
  "Return the buffer containing the interactive keybindings."
  (let* ((kb-buf-name "*Interactive Keybindings*")
         (kb-buf (get-buffer kb-buf-name)))
    (if (not kb-buf)
        (progn (with-current-buffer
                   (find-file-noselect
                    interactive-keybindings-file)
                 (emacs-lisp-mode)
                 (read-only-mode)
                 (rename-buffer kb-buf-name))
               (get-buffer kb-buf-name))
      kb-buf)))

(defun interactive-keybindings--get-keybindings-list ()
  "Read the interactive-keybindings file into a list."
  (let ((kbs (interactive-keybindings--get-buffer))
        (ikbds '()))
    (with-current-buffer kbs
      (goto-char (point-min))
      (ignore-errors
        (while (setq ikbd (eval (read kbs)))
          (push ikbd ikbds))))
    ikbds))


(defun interactive-keybindings--read-key-until (prompt-string)
  "Present PROMPT-STRING and read keys until interactive-keybindings-quit-key is pressed."
  (setq key-sequence (vector))
  (while (progn

           (let* ((key-vector (read-key-sequence-vector
                               prompt-string
                               t
                               t
                               t))
                  (quit? (string-equal
                          (key-description key-vector)
                          (key-description
                           interactive-keybindings-quit-key-vector))))
             (if quit?
                 nil
               (setq key-sequence
                     (vconcat
                      key-sequence
                      key-vector))))))
  key-sequence)

(defun interactive-keybindings--get-active-maps ()
  "Gets the active keymaps."
  (mapcar
   (lambda (x)
     (interactive-keybindings--keymap-symbol x))
   (current-active-maps t)))

(defun interactive-keybindings--get-keymaps ()
  "Get all keymaps."
  (delete-dups
   (append
    (mapcar
     #'interactive-keybindings--keymap-symbol
     (current-active-maps t))
    (interactive-keybindings--get-from-obarray
     #'keymapp)
    )))

(defun interactive-keybindings--keymap-symbol (keymap)
  "Return the symbol to which KEYMAP is bound, or nil if no such symbol exists."
  (catch 'gotit
    (mapatoms (lambda (sym)
                (and (boundp sym)
                     (eq (symbol-value sym) keymap)
                     (not (eq sym 'keymap))
                     (throw 'gotit sym))))))

(defun interactive-keybindings--get-from-obarray (pred)
  "Return all symbols from obarray where PRED is true."
  (let ((ret ()))
    (mapatoms
     (lambda (x)
       (if (funcall pred x)
           (push x ret))))
    ret))

(defun interactive-keybindings--get-funclist ()
  "Gets a list of all commands known to Emacs."
  (interactive-keybindings--get-from-obarray #'commandp))

(defun interactive-keybindings--check-eval (dk)
  "Eval DK in 'condition-case' and warn user on error."
  (condition-case err-info
      (progn (eval dk)
             t)
    ((error)
     (warn
      "Error evaling resulting keybinding expression, %s. Skipping keybinding."
      (cdr err-info))
     nil)))


(provide 'interactive-keybindings)
;;; interactive-keybindings ends here
