(when (boundp 'comp-deferred-compilation)
  (setq comp-deferred-compilation t))

;; -*- lexical-binding: t; -*-
  (setq gc-cons-threshold 402653184
    gc-cons-percentage 0.6)

(setq read-process-output-max (* 1024 1024))

(setq noninteractive t)
(add-hook 'after-init-hook (lambda () (setq noninteractive nil)))

(setq package-enable-at-startup nil
package--init-file-ensured t)

(add-hook
 'after-save-hook
 (lambda ()
   (let ((bn (org-org-export-to-org)))
     (org-babel-tangle-file bn)
     (delete-file bn)))
 nil
 t)

(setq straight-check-for-modifications '(find-when-checking check-on-save))
(setq straight-repository-branch "develop")
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(require 'straight-x)

(defun daniel/get-straight-packages ()
  (interactive)
  (let ((package-plists (straight-x-existing-repos)))
    (mapcar (lambda (package-plist) (plist-get package-plist :package))
            package-plists)))

(defun daniel/straight-installed-p (&optional package-name)
  (interactive "sPackage: ")
  (seq-contains (daniel/get-straight-packages) package-name))

(defun package-installed-p (package-name)
  (daniel/straight-installed-p package-name))

(setq load-prefer-newer t)
(with-eval-after-load 'use-package
  (use-package auto-compile
    :straight t
    :demand t
    :config
    (auto-compile-on-load-mode)
    (auto-compile-on-save-mode)))

(straight-use-package 'use-package)
; Turning off always defer as I use emacs server now
(setq use-package-always-defer nil
      use-package-verbose t)

(use-package which-key
  :straight t
  :defer .1
  :custom
  (which-key-idle-delay .1)
  :config
  (which-key-mode))

(use-package general
  :straight t
  :demand t
  :config
  (general-create-definer daniel/space-leader-def
    :keymaps 'general-override-mode-map
    :states '(normal insert emacs visual)
    :prefix "SPC"
    :non-normal-prefix "M-SPC")
  (general-create-definer daniel/local-leader-def
    :keymaps 'general-override-mode-map
    :states '(normal insert emacs visual)
    :prefix ","
    :non-normal-prefix "M-,"))

(daniel/space-leader-def
  "SPC" #'helm-M-x)

(require 'subr-x)
(straight-use-package 'git)

(defun org-git-version ()
  "The Git version of org-mode.
Inserted by installing org-mode or when a release is made."
  (require 'git)
  (let ((git-repo (expand-file-name
                   "straight/repos/org/" user-emacs-directory)))
    (string-trim
     (git-run "describe"
              "--match=release\*"
              "--abbrev=6"
              "HEAD"))))

(defun org-release ()
  "The release version of org-mode.
  Inserted by installing org-mode or when a release is made."
  (require 'git)
  (let ((git-repo (expand-file-name
                   "straight/repos/org/" user-emacs-directory)))
    (string-trim
     (string-remove-prefix
      "release_"
      (git-run "describe"
               "--match=release\*"
               "--abbrev=0"
               "HEAD")))))

(provide 'org-version)

(use-package
    org
  :straight org-plus-contrib
  :mode (("\\.org$" . org-mode))
  :custom (org-imenu-depth 4)
  (org-html-doctype "html5")
  :config (require 'org-tempo)
  (add-to-list
   'org-structure-template-alist
   '("el" . "src emacs-lisp")))

;;(straight-use-package 'org-plus-contrib) ; or org-plus-contrib if desired. I do desire it
;; Using instead the use-package macro above as that way I can go ahead and defer it and such

(use-package evil-org
  :straight t
  :after (org evil)
  :hook (org-mode . evil-org-mode)
  :config
  (add-hook 'evil-org-mode-hook
      (lambda ()
  (evil-org-set-key-theme)))
  )

(use-package ox-pandoc
  :straight t)

(setq org-confirm-babel-evaluate nil)
(setq enable-local-variables t)

(daniel/local-leader-def
  :keymaps 'org-mode-map
  "e" '(:ignore t :which-key "Edit")
  "e s" #'org-edit-special
  "j" '(:ignore t :which-key "Jump prefix")
  "j l" '(ace-link-org :which-key "Jump to link")
  "j h" '(helm-org-in-buffer-headings :which-key "Jump to heading")
  "p" #'org-property-action
  "," #'org-ctrl-c-ctrl-c
  "T i" #'org-toggle-inline-images
  "c l" #'org-cycle-list-bullet)

(general-define-key
 :keymaps 'org-src-mode-map
 [remap evil-save-and-close] #'org-edit-src-exit
 [remap evil-save-modifier-and-close] #'org-edit-src-exit
 [remap evil-quit] #'org-edit-src-abort)

;; These don't seem to work for some reason
;; Seems to be some problem with when they are loaded, they'll work after tyring to use them twice
(daniel/local-leader-def
  :keymaps 'org-src-mode-map
  "c" #'org-edit-src-exit
  "k" #'org-edit-src-abort)

(with-eval-after-load 'org
      (add-to-list 'org-babel-load-languages '(dot . t)))

(use-package jupyter
  :straight t
  :after 'org-mode
  :hook 'org-mode
  :defer 1
  :config
  (add-to-list 'org-babel-load-languages '(jupyter . t)))

(use-package avy
  :straight t
  :general
  ("C-:" #'avy-goto-char-timer
   "M-g g" #'avy-goto-line))

(use-package ace-link
  :straight t
  :hook after-init-hook)

(use-package rainbow-delimiters
  :straight t
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package evil
  :straight t
  :init
  (setq evil-want-keybinding nil
        evil-respect-visual-line-mode t)
  :demand t
  :config
  (evil-mode 1))

(use-package evil-collection
  :straight t
  :after (evil)
  :demand t
  :custom
  (evil-collection-setup-minibuffer t)
  (evil-collection-company-use-tng nil)
  :config
  (evil-collection-init)
  (evil-collection-helm-setup))

(use-package evil-surround
  :straight t
  :demand t
  :config
  (global-evil-surround-mode 1))

(use-package evil-exchange
  :straight t
  :after evil
  :init
  (evil-exchange-install))

(use-package evil-lion
  :straight t
  :after evil
  :config
  (evil-lion-mode))

(use-package evil-nerd-commenter
   :straight t
:after evil
:defer 1
:config 
(evilnc-default-hotkeys))

(use-package helm
  :straight t
  :demand (daemonp)
  :general
  ("M-x" 'helm-M-x
   [remap find-file] 'helm-find-files)
   (daniel/space-leader-def
      "h" 'helm-command-prefix)
  :config
  (require 'helm-config)

  ;; The default C-x c" is quite close to "C-x C-c", which quits Emacs.
  ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
  ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.

  (global-unset-key (kbd "C-x c"))

  (when (executable-find "curl")
    (setq helm-google-suggest-use-curl-p t))

  (setq helm-split-window-in-side-p t ; open helm buffer inside current window, not occupy whole other window
        helm-move-to-line-cycle-in-source t ; move to end or beginning of source when reaching top or bottom of source.
        helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.
        helm-scroll-amount 8 ; scroll 8 lines other window using M-<next>/M-<prior>
        helm-ff-file-name-history-use-recentf t
        helm-echo-input-in-header-line t)

  (defun spacemacs//helm-hide-minibuffer-maybe ()
    "Hide minibuffer in Helm session if we use the header line as input field."
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
        (overlay-put ov 'window (selected-window))
        (overlay-put ov 'face
                     (let ((bg-color (face-background 'default nil)))
                       `(:background ,bg-color :foreground ,bg-color)))
        (setq-local cursor-type nil))))


  (add-hook 'helm-minibuffer-set-up-hook
            'spacemacs//helm-hide-minibuffer-maybe)

  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)
  (helm-autoresize-mode 1)

  (helm-mode 1))

(defun helm-org-completing-read-setup ()
  (progn
    (add-to-list
     'helm-completing-read-handlers-alist
     '(org-capture . helm-org-completing-read-tags))
    (add-to-list
     'helm-completing-read-handlers-alist
     '(org-set-tags . helm-org-completing-read-tags))))

(use-package helm-org
  :hook (org-mode . #'helm-org-completing-read-setup))

(use-package hydra
  :straight t
  :demand t
  )

(daniel/space-leader-def
  "z" '(hydra-zoom/body
        :which-key "Zoom"))

(defhydra hydra-zoom ()
  "zoom"
  ("i" #'text-scale-increase "in")
  ("o" #'text-scale-decrease "out")
  ("r" (text-scale-set 0) "reset")
  ("q" nil "cancel"))

(use-package major-mode-hydra
  :straight t
  :demand t
  :general
  ;; (daniel/space-leader-def
  ;;   "m" #'major-mode-hydra)
  )

(use-package projectile
  :straight t
  :general
  (daniel/space-leader-def
    "p" '(:keymap projectile-command-map :which-key "Projectile" :package projectile))
  :custom
  (projectile-enable-caching t)
  :config
  (push ".exercism" projectile-project-root-files)
  (projectile-mode 1))

(use-package helm-projectile
  :straight t
  :after (projectile helm)
  :config
  (helm-projectile-on))

(use-package treemacs
  :straight t
  :custom
  (treemacs-collapse-dirs 3)
  (treemacs-project-follow-cleanup t)
  :general
  (daniel/space-leader-def
    "t" #'treemacs)
  :config
  (treemacs-git-mode 'deferred)
  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode t)
  )

(use-package treemacs-evil
  :straight t
  :after treemacs evil
  )

(use-package treemacs-projectile
  :straight t
  :after treemacs projectile
  )

(use-package treemacs-icons-dired
  :straight t
  :after treemacs dired
  :config
  (treemacs-icons-dired-mode)
  )

(use-package treemacs-magit
  :straight t
  :after treemacs magit
  )

(use-package yasnippet
  :straight t
  :demand t
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :straight t)

(use-package company-yasnippet
  :after (company yasnippet))

(use-package company
  :straight t
  :demand t
  :custom 
  (company-minimum-prefix-length 1)
  (company-idle-delay .1)
  ;; :general
  ;; (general-define-key :keymaps 'company-active-map "TAB" #'company-complete-common-or-cycle
  ;;                     "RET" #'company-complete-selection)
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  )

(use-package company-quickhelp
  :straight t
  :demand t
  :custom (company-quickhelp-delay 0.2)
  :config
  (company-quickhelp-mode))

(use-package edit-server
  :straight t
  :defer 1
  :general
  (daniel/local-leader-def :keymaps 'edit-server-mode-map "c" #'edit-server-done
    "k" #'edit-server-abort)
  :config
  (edit-server-start))

(use-package magit
  :straight t
  :custom
  (magit-diff-refine-hunk 'all)
  (magit-diff-refine-ignore-whitespace t)
  (magit-diff-paint-whitespace t)
  :general
  ("C-x g" #'magit-status)
  (daniel/space-leader-def
    "g" '(:ignore t :which-key "Git")
    "g s" #'magit-status)

  (daniel/local-leader-def
    :keymaps 'with-editor-mode-map
    "c" 'with-editor-finish
    "k" 'with-editor-cancel))

(use-package forge
  :straight t
  :after magit
  :custom
  (emacsql-sqlite-data-root (straight--repos-dir "emacsql"))
  :demand t)

(use-package evil-magit
  :straight t
  :after (evil magit)
  :hook (magit-mode . evil-magit-init))

(use-package git-timemachine
  :straight t
  :commands git-timemachine
  :general
  (daniel/space-leader-def
    "g t" #'git-timemachine))

(use-package minions
  :straight t
  :demand t
  :config
  (minions-mode))

(use-package exec-path-from-shell
  :straight t
  :defer 1
  :config
  (exec-path-from-shell-initialize))

(savehist-mode 1)
(add-to-list 'savehist-additional-variables 'kill-ring)
(setq
 desktop-save t
 desktop-restore-frames t
 desktop-load-locked-desktop t)
(desktop-save-mode -1)
;; (add-hook 'kill-emacs-hook #'desktop-clear)

(defun daniel/clean-tramp-stuff ()
  "Clean up tramp stuff so my system can shut down even if I opened something in tramp."
  (when (featurep 'tramp)
    (progn
      (tramp-cleanup-all-connections)
      (tramp-cleanup-all-buffers))))

(add-hook 'kill-emacs-hook #'daniel/clean-tramp-stuff)
(defun daniel/restart-emacs ()
  (interactive)
  (progn
    (remove-hook 'kill-emacs-hook #'desktop-clear)
    (restart-emacs)))

(defun daniel/clear-and-quit ()
  "Clear the desktop so that we don't wind up in the same file everytime we start emacs"
  (interactive)
  (progn (desktop-clear) (save-buffers-kill-emacs)))
(add-hook 'kill-emacs-hook #'daniel/kill-all-other-buffers)
(use-package restart-emacs
  :straight t
  :defer 1
  :custom
  (restart-emacs-restore-frames t)
  :general
  (daniel/space-leader-def
    :infix "q"
    "" '(:ignore t :which-key "Quitting")
    "r" #'restart-emacs
    "q" #'daniel/clear-and-quit
    ))

(defcustom daniel-config-file
  (concat user-emacs-directory "config.org")
  "Location of my config file."
  :type 'file)

(defun daniel/open-config-file ()
  (interactive)
  (find-file daniel-config-file))

(defun daniel/reload-config ()
  (interactive)
  (progn
    (message "Reload init.el...")
    (load-file (concat user-emacs-directory "init.el"))
    (message "Reload init.el...Done!")
    ))

(fset 'yes-or-no-p 'y-or-n-p)

(global-visual-line-mode)
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

(global-auto-revert-mode t)

(let ((backup-dir (concat user-emacs-directory ".backups")))
(setq backup-by-copying t
      backup-directory-alist `(("." . ,backup-dir))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t))

(save-place-mode 1)

(defun daniel/kill-all-other-buffers ()
  "Kill all buffers not currently shown in a window"
  (interactive)
  (dolist (buf (buffer-list))
    (unless (get-buffer-window buf 'visible) (kill-buffer buf))))

(use-package no-littering
  :straight t
  :demand t
  :config
  (setq auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))
  custom-file (no-littering-expand-etc-file-name "custom.el")))

(add-hook 'prog-mode-hook #'hs-minor-mode)

(electric-pair-mode +1)

(defun indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

(defun daniel/format-buffer ()
  (interactive)
  (require 'format-all)
  (save-excursion
    (cond ((bound-and-true-p lsp-mode)
           (lsp-format-buffer))
          ((car (format-all--probe))
           (format-all-buffer))
          (t (indent-buffer)))))

;; (set-face-attribute 'default nil :height 160)

(defun daniel/close-help-buffer ()
  (interactive)
  (let ((hb (seq-find
             (lambda (buffer)
               (string-equal
                (buffer-name buffer)
                "*Help*"))
             (buffer-list))))
    (kill-buffer hb)))

(global-display-line-numbers-mode t)

(setq dired-auto-revert-buffer t)

(setq-default
 tab-width 2
 evil-shift-width 2
 indent-tabs-mode nil
 truncate-lines t)
 (setq c-basic-offset 2)

(use-package doom-themes
  :straight t
  :demand t
  :config
   ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
  doom-themes-enable-italic t) ; if nil, italics is universally disabled

  ;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each theme
  ;; may have their own settings.
  (load-theme 'doom-gruvbox t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)

  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
)

(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)

(use-package lsp-mode
  :general
  ("M-RET" #'lsp-execute-code-action)
  :custom
  (lsp-restart 'auto-restart)
  (lsp-print-io t)
  :straight t
  :after (yasnippet)
  :hook
  (lsp-mode . lsp-enable-which-key-integration)
  :commands lsp
  :config
  (lsp-lens-mode 1))

(use-package lsp-ui
  :straight t
  :commands lsp-ui-mode
  :after lsp
  )

(use-package helm-lsp
  :straight t
  :after (lsp helm)
  :commands (helm-lsp-workspace-symbol helm-lsp-global-workspace-symbol))

(use-package lsp-treemacs
  :straight t
  :commands lsp-treemacs-errors-list)

(defun daniel/remove-all-lsp-session-folder ()
   (interactive)
   (mapc #'lsp-workspace-folders-remove (lsp-session-folders (lsp-session))))

(general-define-key
 :states 'normal
 "g d" (general-predicate-dispatch 'evil-goto-definition
         lsp-mode 'lsp-find-definition))

(use-package dap-mode
  :straight t
  :after lsp
  :hook lsp-mode
  :config
  (dap-mode 1)
  (dap-tooltip-mode 1)
  (dap-ui-mode 1))

(defun my/window-visible (b-name)
  "Return whether B-NAME is visible."
  (-> (-compose 'buffer-name 'window-buffer)
      (-map (window-list))
      (-contains? b-name)))

(defun my/show-debug-windows (session)
  "Show debug windows."
  (let ((lsp--cur-workspace (dap--debug-session-workspace session)))
    (save-excursion
      ;; display locals
      (unless (my/window-visible dap-ui--locals-buffer)
        (dap-ui-locals))
      ;; display sessions
      (unless (my/window-visible dap-ui--sessions-buffer)
        (dap-ui-sessions)))))

(add-hook 'dap-stopped-hook 'my/show-debug-windows)

(defun my/hide-debug-windows (session)
  "Hide debug windows when all debug sessions are dead."
  (unless (-filter 'dap--session-running (dap--get-sessions))
    (and (get-buffer dap-ui--sessions-buffer)
         (kill-buffer dap-ui--sessions-buffer))
    (and (get-buffer dap-ui--locals-buffer)
         (kill-buffer dap-ui--locals-buffer))))

(add-hook 'dap-terminated-hook 'my/hide-debug-windows)

(require 'dap-gdb-lldb)
(dap-gdb-lldb-setup)

(use-package flycheck
  :straight t
  :general
  (daniel/space-leader-def
    "e" '(:keymap flycheck-command-map :which-key "Flycheck" :package flycheck))
  :init
  (global-flycheck-mode))

(define-derived-mode cfn-mode json-mode
  "Cloudformation"
  "Cloudformation template mode")

(add-to-list 'auto-mode-alist '(".template\\'" . cfn-mode))
(with-eval-after-load 'flycheck
  (flycheck-define-checker cfn-lint
    "A cloudformation linter using cfn-python-lint

See URL https://github.com/awslabs/cfn-python-lint"
    :command ("cfn-lint" "-f" "parseable" source)
    :error-patterns (
         (warning line-start (file-name) ":" line ":" column
            ":" (one-or-more digit) ":" (one-or-more digit) ":"
            (id "W" (one-or-more digit)) ":" (message) line-end)
         (error line-start (file-name) ":" line ":" column
            ":" (one-or-more digit) ":" (one-or-more digit) ":"
            (id "E" (one-or-more digit)) ":" (message) line-end)
         )
    :modes (cfn-mode)
    )
  (add-to-list 'flycheck-checkers 'cfn-lint))

(use-package format-all
  :straight t
  :commands format-all-buffer)

(use-package origami
  :straight t
  :defer 1
  :config
  (global-origami-mode))

(use-package lsp-origami
  :straight t
  :after origami
  :hook (origami-mode-hook . #'lsp-origami-mode))

(use-package sly
  :demand t
  :custom (inferior-lisp-program  "sbcl")
  :straight t)

(use-package sly-named-readtables
  :after sly
  :straight t)

(use-package sly-asdf
  :after sly
  :straight t)

(use-package sly-quicklisp
  :after sly
  :straight t)

(use-package
    lispy
  :straight t
  :hook
  (emacs-lisp-mode . lispy-mode)
  (lisp-mode . lispy-mode))


(use-package
  lispyville
  :straight t
  :after lispy
  :init (setq lispyville-motions-put-into-special
      t
      lispyville-commands-put-into-special
      t)
  :hook (lispy-mode . lispyville-mode)
  :config (lispyville-set-key-theme
       '(operators
         c-w
         (escape insert)
         (additional-movement
      normal
      visual
      motion)
         commentary
         additional-insert
         mark
         mark-toggle)))

(use-package ccls
  :straight t
  :hook ((c-mode c++-mode objc-mode) . (lambda () (require 'ccls) (lsp))))
(with-eval-after-load 'org
  (add-to-list 'org-babel-load-languages '(C . t)))

(use-package markdown-mode
  :straight t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(add-hook 'python-mode-hook (lambda () (progn (lsp) (require 'dap-python))))

(use-package lsp-python-ms
  :straight (:host github :repo "andrew-christianson/lsp-python-ms")
  :hook (python-mode . lsp)
  :config
  (setq lsp-python-ms-executable mspyls)
  )

(use-package lpy
  :straight (:host github :repo "abo-abo/lpy")
  :hook (python-mode . lpy-mode))

(use-package pyvenv
  :straight t
  :commands pyvenv-activate
  )

(use-package live-py-mode
  :straight t
  :commands #'live-py-mode)

(use-package lsp-java
  :straight t
  :demand t
  :config
  (require 'dap-java))

(add-hook 'java-mode-hook #'lsp)

(use-package scala-mode
  :straight t
  :mode "\\.s\\(cala\\|bt\\)$")

(use-package sbt-mode
  :straight t
  :commands sbt-start sbt-command
  :config
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map))

   (add-hook 'scala-mode-hook #'lsp)

(use-package csharp-mode
  :straight t
  :config
  (electric-pair-local-mode 1)
  )

(add-hook 'csharp-mode-hook #'lsp)

;; (use-package omnisharp
;;   :straight t
;;   :hook (csharp-mode)
;;   :config
;;   (add-to-list 'company-backends #'company-omnisharp)
;;   (omnisharp-mode)

(use-package fsharp-mode
  :straight t
  :mode ("\\.fs[iylx]?$" . fsharp-mode)
  :custom
  (inferior-fsharp-program "fsharpi --readline-")
  (fsharp-compiler "fsharpc")
  (fsharp-doc-idle-delay 0.2))

  (add-hook 'fsharp-mode-hook #'lsp)

(use-package elm-mode
  :straight t)

(setq lsp-elm-server-install-dir "/home/daniel/Src/github/elm-language-server/")
(add-hook 'elm-mode-hook #'lsp)

(use-package elixir-mode
  :straight t)

(setq lsp-clients-elixir-server-executable "/home/daniel/Src/github/elixir-ls/release/language_server.sh")

(add-hook 'elixir-mode-hook #'lsp)

(use-package js2-mode
  :straight t
  :init
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode)))

(use-package tide
  :straight t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(use-package web-mode
  :straight t
  :init
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tsx\\'" . typescript-mode))
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
  (setq web-mode-enable-current-element-highlighting t))

(use-package protobuf-mode
  :straight t)

(use-package yaml-mode
  :straight t
  :mode ("\\.y[a]?ml$" . yaml-mode))

(use-package geiser
  :straight t)

(use-package rustic
  :straight t
  :custom
  (rustic-lsp-server 'rust-analyzer))

(setq lsp-rust-server 'rust-analyzer)
(add-hook 'rustic-mode-hook #'lsp)

(use-package dhall-mode
:straight t
:mode "\\.dhall\\'")

(add-to-list 'lsp-language-id-configuration '(dhall-mode . "dhall"))

(lsp-register-client
 (make-lsp-client :new-connection (lsp-stdio-connection "dhall-lsp-server")
                  :major-modes '(dhall-mode)
                  :server-id 'dhall-lsp-server))

(use-package powershell
  :straight t
  :mode ("\\.ps1\\'" . powershell-mode)
  )

(add-hook 'powershell-mode-hook #'lsp)

(use-package go-mode
  :straight t
  :mode ("\\.go\\'" . go-mode))

(add-hook 'go-mode-hook 'lsp-deferred)

(use-package kubernetes
  :straight t
  :commands (kubernetes-overview)
  :config
  (require 'kubernetes-evil))

(use-package kubernetes-evil
  :straight t
  :after kubernetes)

(use-package kubel
  :straight t
  :commands #'kubel)

(use-package dockerfile-mode
  :straight t
  :mode  ("Dockerfile\\'" . dockerfile-mode))

(use-package docker
  :straight t
  :custom
  (docker-image-run-arguments '("-i" "-t" "--rm")))

(use-package leetcode
  :straight t
  :custom
  (leetcode-account "Daniel-V1")
  (leetcode-prefer-language "rust")
  (leetcode-prefer-sql "postgres"))

(use-package vterm
  :straight t
  :custom
  (vterm-buffer-name-string "vterm %s"))

(let (vterm-install)
  (require 'vterm))

(use-package csv-mode
:straight t)

(use-package deadgrep
:straight t
:commands #'deadgrep)

(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-diff-options "-w")

(use-package string-inflection
  :straight t
  :commands string-inflection-all-cycle)

(use-package pkgbuild-mode
:straight t
  :mode ("\\PKGBUILD\\'" . pkgbuild-mode))

(use-package pandoc-mode
  :straight t)

(use-package nix-mode
    :straight t
    :mode ("\\.nix\\'" "\\.nix.in\\'"))
  (use-package nix-drv-mode
    :mode "\\.drv\\'")
  (use-package nix-shell
    :commands (nix-shell-unpack nix-shell-configure nix-shell-build))
  (use-package nix-repl
    :commands (nix-repl))
(use-package nix-company
    :commands nix-company
    :hook (nix-mode . (lambda ()
                        (setq-local company-backends '(nix-company)))))

(use-package nixos-options
  :straight t)

(use-package company-nixos-options
  :straight t)

(setq nix-ls-path "/Users/daniel.vetter/src/personal/github/nix-language-server/target/debug/nix-language-server")
(lsp-register-client
 (make-lsp-client :new-connection (lsp-stdio-connection nix-ls-path)
                  :major-modes '(nix-mode)
                  :server-id 'nix
                  ))
(add-to-list 'lsp-language-id-configuration '(nix-mode . "nix"))

(use-package request
  :straight t)

(defun spacemacs/rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let* ((name (buffer-name))
         (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let* ((dir (file-name-directory filename))
             (new-name (read-file-name "New name: " dir)))
        (cond ((get-buffer new-name)
               (error "A buffer named '%s' already exists!" new-name))
              (t
               (let ((dir (file-name-directory new-name)))
                 (when (and (not (file-exists-p dir)) (yes-or-no-p (format "Create directory '%s'?" dir)))
                   (make-directory dir t)))
               (rename-file filename new-name 1)
               (rename-buffer new-name)
               (set-visited-file-name new-name)
               (set-buffer-modified-p nil)
               (when (fboundp 'recentf-add-file)
                 (recentf-add-file new-name)
                 (recentf-remove-if-non-kept filename))
               (when (require 'projectile nil 'noerror) (projectile-project-p)
                     (call-interactively #'projectile-invalidate-cache))
               (message "File '%s' successfully renamed to '%s'" name (file-name-nondirectory new-name))))))))

(daniel/space-leader-def
  :infix "f"
  "" '(:ignore t :which-key "Files")
  "f" #'helm-find-files
  "s" #'save-buffer
  "r" #'spacemacs/rename-current-buffer-file
  "c" '(:which-key "My Config")
  "c e" #'daniel/open-config-file
  "c r" #'daniel/reload-config)

(daniel/space-leader-def
  :infix "b"
  "" '(:ignore t :which-key "Buffers")
  "b" #'helm-buffers-list
  "d" #'kill-current-buffer
  "p" #'evil-prev-buffer
  "n" #'evil-next-buffer
  "=" #'daniel/format-buffer)

(daniel/space-leader-def
  :infix "w"
  "" '(:ignore t :which-key "Windows")
  "h" #'evil-window-left
  "l" #'evil-window-right
  "j" #'evil-window-down
  "k" #'evil-window-up
  "J" #'evil-window-top
  "/" #'evil-window-vsplit
  "-" #'evil-window-split
  "d" #'delete-window
  "m" #'delete-other-windows
  "a" #'ace-window)

(daniel/space-leader-def
  :infix "j"
  "" '(:ignore t :which-key "Jump to")
  "l" #'ace-link
  "c" #'avy-goto-char-timer
  "i" #'helm-imenu)


