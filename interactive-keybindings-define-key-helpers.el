;;; package --- Interactive-keybindings -*- lexical-binding: t; -*-
;;; Commentary:

;;; Code:
(require 'interactive-keybindings)
(require 'dash)

(defcustom interactive-keybindings-define-key-out-file (f-join
                                                        user-emacs-directory
                                                        "define-key-out.el")
  "File which store the output of running interactive-keybindings-convert-file-keybindings."
  :type 'file
  :group 'interactive-keybindings)

(defun interactive-keybindings--convert-keybinding-to-define-key-list (interactive-keybinding)
  "Turn INTERACTIVE-KEYBINDING into a 'define-key' list for each keymap."
  (let ((keymaps (oref interactive-keybinding keymaps))
        (command (oref interactive-keybinding command))
        (key-seq (oref interactive-keybinding key-sequence)))
    (mapcar
     (lambda (keymap)
       `(define-key
          ,keymap
          (kbd ,(key-description key-seq))
          #',command))
     keymaps)))

(defun interactive-keybindings-define-key-helpers-translate-file-to-define-key-file ()
  "Convert the interactive-keybindings in interactive-keybindings-file to 'define-key' statements."
  (interactive)
  (let ((keybinding-list
         (-filter
          (lambda (kb)
            (same-class-p
             kb
             'interactive-keybindings-interactive-keybinding))
          (interactive-keybindings--get-keybindings-list))))
    (with-temp-buffer
      (erase-buffer)
      (mapc
       (lambda (keybinding)
         (let ((dks (interactive-keybindings--convert-keybinding-to-define-key-list
                     keybinding)))
           (mapc
            (lambda (dk)
              (when (interactive-keybindings--check-eval
                   dk)
                  (insert (format "%s\n" dk))))
            dks)))
       keybinding-list)
      (write-file
       interactive-keybindings-define-key-out-file))))


(defun interactive-define-key ()
  "A function which hopefully will allow me to define keybindings on the fly and store them to a file."
  (interactive)
  (let* ((ikbd (interactive-keybindings--get-keybinding))
         (conflicts (interactive-keybindings--check-for-conflicts
                     (oref ikbd key-sequence)
                     (oref ikbd keymaps))))
    (if conflicts
        (cl-loop
         for
         conflict
         in
         conflicts
         do
         (progn
           (interactive-keybindings--keybind-conflict-to-warning
            conflict)
           (message "There were conflicts")
           ))
      (let* ((dks (when ikbd
                    (interactive-keybindings--convert-keybinding-to-define-key-list
                     ikbd)))
             (eval-results (remove
                            nil
                            (mapcar
                             #'interactive-keybindings--check-eval
                             dks))))
        (when (and dks eval-results)
          (interactive-keybindings--add-keybinding-to-file
           ikbd))))))

;;; interactive-keybindings-define-key-helpers ends here
