(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-idle-delay 0.1)
 '(company-minimum-prefix-length 1)
 '(company-quickhelp-delay 0.2)
 '(evil-collection-company-use-tng nil)
 '(evil-collection-setup-minibuffer t)
 '(magit-diff-paint-whitespace t t)
 '(magit-diff-refine-hunk 'all t)
 '(magit-diff-refine-ignore-whitespace t t)
 '(org-html-doctype "html5")
 '(org-imenu-depth 4)
 '(projectile-enable-caching t)
 '(restart-emacs-restore-frames t)
 '(safe-local-variable-values
   '((eval save-excursion
           (org-babel-goto-named-src-block "add-tangle-after-save")
           (org-babel-execute-src-block))))
 '(treemacs-collapse-dirs 3)
 '(treemacs-project-follow-cleanup t)
 '(which-key-idle-delay 0.1))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
